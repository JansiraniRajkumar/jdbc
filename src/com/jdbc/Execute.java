package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Execute {
	
	public static void main(String args[]) throws ClassNotFoundException, SQLException {
		String sql = "UPDATE employee SET name=?  WHERE id=?";
	
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/emp", "root", "root");
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, "jansirani");
			statement.setInt(2, 1);
			 
			int rowsUpdated = statement.executeUpdate();
			if (rowsUpdated > 0) {
			    System.out.println("An existing user was updated successfully!");
			} 
			
	}

}

