package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Employee {
	
	public static void main(String args[]) throws ClassNotFoundException, SQLException {
		String sql = "DELETE FROM employee WHERE name=?";
	
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/emp", "root", "root");
			
			 
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, "jansirani");
			 
			int rowsDeleted = statement.executeUpdate();
			if (rowsDeleted > 0) {
			    System.out.println("A user was deleted successfully!");
			}
	}

}
