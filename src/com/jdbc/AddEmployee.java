package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddEmployee {
	
	public static void main(String args[]) throws ClassNotFoundException, SQLException {
		String sql = "INSERT INTO employee (id, name, age) VALUES (?, ?, ?)";
	
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/emp", "root", "root");
			
			 
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setInt(1, 5);
			statement.setString(2, "Diwahar");
			statement.setInt(3, 19);
			
			 
			int rowsInserted = statement.executeUpdate();
			if (rowsInserted > 0) {
			    System.out.println("A new user was inserted successfully!");
			}
	}
}
